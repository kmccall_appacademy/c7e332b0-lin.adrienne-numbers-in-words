class Fixnum

  ONES = %w[zero one two three four five six seven eight nine]
  TEENS = %w[ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen]
  TENS = %w[twenty thirty forty fifty sixty seventy eighty ninety]
  LARGER = [['hundred', 2],
            ['thousand', 3],
            ['million', 6],
            ['billion', 9],
            ['trillion', 12]]

  def in_words
    number_str = ''

    if self < 10
      number_str << ONES[self]
    elsif self < 20
      number_str << TEENS[self - 10]
    elsif self < 100
      if (self % 10).zero?
        number_str << TENS[(self / 10) - 2]
      else
        number_str << TENS[(self / 10) - 2] + ' ' + (self % 10).in_words
      end
    else
      mag_pair = self.find_magnitude
      mag = 10**(mag_pair[1])
      number_str << (self/mag).in_words + ' ' + mag_pair[0] unless (self/mag).zero?
      number_str << ' ' + (self % mag).in_words unless (self % mag).zero?
    end
    number_str
  end

  def find_magnitude
    LARGER.take_while { |mag| 10**mag[1] <= self }.last
  end
end
